import { LightningElement } from 'lwc';
export default class MyFirstComponent extends LightningElement {
  greeting = 'Dear';
  changeHandler(event) {
    this.greeting = event.target.value;
  }
}